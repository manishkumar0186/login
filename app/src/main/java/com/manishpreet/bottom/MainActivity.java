package com.manishpreet.bottom;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.eightbitlab.bottomnavigationbar.BottomBarItem;
import com.eightbitlab.bottomnavigationbar.BottomNavigationBar;

public class MainActivity extends AppCompatActivity {
BottomNavigationBar navigation;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        navigation=findViewById(R.id.bottom_bar);
        BottomBarItem one =new BottomBarItem(R.drawable.home,R.string.home);
        BottomBarItem two =new BottomBarItem(R.drawable.veg,R.string.veg);
        BottomBarItem three =new BottomBarItem(R.drawable.user,R.string.user);
        BottomBarItem four =new BottomBarItem(R.drawable.home,R.string.home);
        BottomBarItem fifth =new BottomBarItem(R.drawable.veg,R.string.veg);


        navigation.addTab(one);
        navigation.addTab(two);
        navigation.addTab(three);
        navigation.addTab(four);
        navigation.addTab(fifth);



        addFragment(new FragmentHome());
        navigation.setOnSelectListener(new BottomNavigationBar.OnSelectListener() {
            @Override
            public void onSelect(int position) {
                switch (position)
                {
                    case 0:
                    {
                        replaceFragment(new FragmentHome());
                    }
                    case 1:
                    {
                        replaceFragment(new FragmentVeg());
                        break;
                    }
                    case 2:
                    {
                        replaceFragment(new FragmentUser());
                        break;
                    }
                }
            }
        });

            }
public void  addFragment(Fragment fragment)
{
    FragmentTransaction transaction=getSupportFragmentManager().beginTransaction();
    transaction.add(R.id.container,fragment);
    transaction.commit();
}
    public void  replaceFragment(Fragment fragment)
    {
        FragmentTransaction transaction=getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container,fragment);
        transaction.commit();
    }


}
